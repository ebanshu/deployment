Install ebanshu3.0 step by step on centOS 6.0+
Teambition link: [https://www.teambition.com/project/55755ab811eb07163aede4fc/posts/post/559e601db5e9f593727713e8](Link URL)

Important Notes:
- You should manually download the packeges/scripts if they cannot be downloaded. 
- 


I. Install server environment (For new machine)

1.1. Get the "install_server.sh" script from server. e.g.

#wget http://www.ebanshu.com/install/deployment/install_server.sh

// Modify install_server.sh
// Replace "download_prefix" to reachable host
#download_prefix="http://www.ebanshu.com/install/deployment"

1.2. Modify install_server.sh to set "download_prefix" as a reachable host

// Replace "download_prefix" to reachable host
#download_prefix="http://www.ebanshu.com/install/deployment"

1.3. Execute the install_server.sh

// Using root to install 
#sh install_server.sh

II. Deploy daemon services 

2.1. Create deploy user which should be different from current exist users, and set a password for the new user

// Add user
#useradd newuser
#passwd newuser

2.2. Go the home directory of new user and fetch the scripts of configuration

// 
#cd /home/newuser
#wget http://www.ebanshu.com/install/deployment/fetch_configs.sh
#chmod +x fetch_configs.sh

2.3. Modify fetch_configs.sh and fetch configure files for deployment

// Mofidy $username to the username created before
#username=newuser

// Replace "download_prefix" to reachable host
#download_prefix="http://www.ebanshu.com/install/deployment"

// fetch all configure files
#cd /home/newuser
#./fetch_configs.sh

2.4. Modify the configuration file for deployment

#cd /home/newuser/deployment

// all the ports should set to the values that haven't been used
#vim server_config.conf

2.5. Fetch and execute the services deployment script

#cd /home/newuser/deployment
#wget http://www.ebanshu.com/install/deployment/deploy_services.sh

// Replace "download_prefix" to reachable host
#download_prefix="http://www.ebanshu.com/install/deployment"

#chmod +x deploy_services.sh
#./deploy_services.h

III. Deploy apps 

3.1 Go the deployment directory and execute deploy_app.sh

// Deploy apps
#su newuser
$cd /home/newuser/deployment
$./deploy_app.sh


